const usersData = require('./1-users_1.cjs');

function interestedInVdoGames(usersData){
    let interestedInVdoGames = []
    if(typeof(usersData) === "object" && typeof(usersData) !== null){
        for(let user in usersData){
            if(Array.isArray(usersData[user].interests)){
                for(let interest of usersData[user].interests){
                    if(interest.includes("Video Games")){
                        interestedInVdoGames.push(user);
                    }
                }
                
            }else{
                return [];
            }
        }
        return interestedInVdoGames;
    }
    else{
        return {};
    }
    
}
console.log(interestedInVdoGames(usersData));

module.exports = interestedInVdoGames; 