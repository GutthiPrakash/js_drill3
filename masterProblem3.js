const usersData = require('./1-users_1.cjs');

function userWithMasterDegree(usersData){   
    let userWithMasters = []
    if(typeof(usersData) === "object" && typeof(usersData) !== null){
        for(let user in usersData){
            if(usersData[user].qualification === "Masters"){
                userWithMasters.push(user);
            }
        }
        return userWithMasters;
    }
    else{
        return {};
    }
}
console.log(userWithMasterDegree(usersData))

module.exports = userWithMasterDegree;