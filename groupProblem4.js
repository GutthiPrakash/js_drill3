const usersData = require('./1-users_1.cjs');

function groupByLanguage(usersData){
    let javaScript = []
    let goLang = []
    let python = []
    if(typeof(usersData) === "object" && typeof(usersData) !== null){   
        for(let user in usersData){
            if(usersData[user].desgination.includes("Javascript")){
                javaScript.push(user)
            }
            else if(usersData[user].desgination.includes("Golang")){
                goLang.push(user)
            }
            else if(usersData[user].desgination.includes("Python")){
                python.push(user)
            }
        }
        console.log(`Javascript : ${javaScript}`)
        console.log(`Golang : ${goLang}`)
        console.log(`Python : ${python}`)
    }
    else{
        console.log({});
    }
}
// groupByLanguage(usersData)


module.exports = groupByLanguage;