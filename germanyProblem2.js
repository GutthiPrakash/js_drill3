const usersData = require('./1-users_1.cjs');

function stayingInGermany(usersData){
    let stayingInGermany = [];
    if(typeof(usersData) === "object" && typeof(usersData) !== null){
        for(let user in usersData){
            if(usersData[user].nationality == "Germany"){
                stayingInGermany.push(user);
            }
        }
        return stayingInGermany;
    }
    else{
        return {};
    }
}
console.log(stayingInGermany(usersData))

module.exports = stayingInGermany;